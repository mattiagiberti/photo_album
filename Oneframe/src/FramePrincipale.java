import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.border.LineBorder;

public class FramePrincipale  extends JFrame implements ActionListener{

	private static final int LARGHEZZA = 1344;
	private static final int ALTEZZA = 756;
	private JMenuItem item_resetta;
	private JMenuItem item_aggiungi_categoria;
	private JPanel content_pane;
	private Album album;
	private CategoriePannello catpan;
	private JScrollPane scroll;
	
	public FramePrincipale(Album album){
		
		this.album = album;
		
		//menu bar
		JMenuBar menu_bar = new JMenuBar();
		JMenu item_menubar_opzioni = new JMenu("OPZIONI");
		JMenu item_menubar_categoria = new JMenu("CATEGORIA");
		
		item_resetta = new JMenuItem("Elimina Album");
		item_aggiungi_categoria = new JMenuItem("Aggiungi categoria");
		
		//item_menubar_opzioni.add(item_salva);
		item_menubar_opzioni.add(new JSeparator(JSeparator.HORIZONTAL));
		item_menubar_opzioni.add(item_resetta);
		item_menubar_categoria.add(item_aggiungi_categoria);
		item_menubar_opzioni.setBorder(new LineBorder(Color.blue,2));
		item_menubar_categoria.setBorder(new LineBorder(Color.blue,2));
		menu_bar.add(item_menubar_opzioni);
		menu_bar.add(item_menubar_categoria);
		
		this.setJMenuBar(menu_bar);
		
		item_resetta.addActionListener(this);
		item_aggiungi_categoria.addActionListener(this);
		
		catpan = new CategoriePannello(this, album);
		aggiungiscroll(catpan);
		
		
		
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	

	@Override
	public void actionPerformed(ActionEvent evento) {
		
		if(evento.getSource() == this.item_aggiungi_categoria) {
			JFrame frame_aggiungi_categoria = new JFrame("Aggiungi una categoria all'album");
			Pannello_nuova_categoria panel = new Pannello_nuova_categoria(album, frame_aggiungi_categoria, this);
			frame_aggiungi_categoria.add(panel);
			frame_aggiungi_categoria.setSize(450,500);
			frame_aggiungi_categoria.setLocationRelativeTo(null);
			frame_aggiungi_categoria.setResizable(false);
			frame_aggiungi_categoria.setVisible(true);
		}
		
		if(evento.getSource() == this.item_resetta) {
			int risposta = JOptionPane.showConfirmDialog(null, "Sei sicuro di voler eliminare l'album?", "Elimina album", JOptionPane.YES_NO_OPTION);
			if(risposta == 0) {
				album.resettaAlbum();
				try {
					album.saveOnFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
				this.dispose();
				Main.main(null);
			}
						
		}
	}
		

	public void cambiaPanello(JPanel daaggiungere) {
		this.remove(content_pane);
		content_pane = new JPanel();
		aggiungiscroll(daaggiungere);
		this.add(content_pane);
		this.revalidate();
		this.repaint();		
	}

	private void aggiungiscroll(JPanel pannellodaaggiungere) {
		scroll = new JScrollPane(pannellodaaggiungere);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setBounds(0, 0, LARGHEZZA, ALTEZZA);
		pannellodaaggiungere.setAutoscrolls(true);
		content_pane = new JPanel(new BorderLayout());
		content_pane.add(scroll);
		this.add(content_pane);
	
	}
	
	
	public void aggiungiPanelCategoria (String nome) {
		catpan.aggiungiPanelCategoria(nome);
	}
}
