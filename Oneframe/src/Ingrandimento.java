import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Ingrandimento extends JPanel implements ActionListener{

		private JLabel lblimg;
		private JPanel panel;
		private JButton indietro;
		private FramePrincipale frame;
		private GalleriaPannello galleria;
		

		public Ingrandimento(GalleriaPannello galleria, String path, FramePrincipale frame) {
			this.galleria = galleria;	
			this.frame = frame;
			indietro = new JButton("Torna alla galleria");
			panel = new JPanel();
			lblimg = new JLabel();
			lblimg.setSize(1300, 700);
			aggiungialabel(path, lblimg);
			indietro.addActionListener(this);
			panel.add(indietro);
			panel.add(lblimg);
			this.add(panel);	
		}
		

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == indietro) {
				frame.cambiaPanello(galleria);
			}
			
		}

		private void aggiungialabel(String path, JLabel imglabel) {
			ImageIcon imgIcon = new ImageIcon();
			Boolean raised_expection = false;
			try {
				BufferedImage img = ImageIO.read(new File(path));
				Image dimg = img.getScaledInstance(imglabel.getWidth(), imglabel.getHeight(), Image.SCALE_SMOOTH);
				imgIcon = new ImageIcon(dimg);
			} catch(Exception ex) {
				raised_expection = true;
			}

			if(raised_expection) {
				try {
					URL url = new URL(path);
					Image image = ImageIO.read(url);
					image = image.getScaledInstance(imglabel.getWidth(), imglabel.getHeight(), Image.SCALE_SMOOTH);
					imgIcon = new ImageIcon(image);
				} catch(Exception ex) {
					ex.printStackTrace();
				}	
			}	

			imglabel.setIcon(imgIcon);
		}
}
