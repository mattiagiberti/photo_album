import java.io.Serializable;

public class Immagine implements Serializable{
	
	private String percorso_immagine;
	
	//COSTRUTTORE
	public Immagine(String path) {
		setPercorso_immagine(path);
	}
	
	//getter & setter
	public String getPercorso_immagine() {
		return percorso_immagine;
	}

	public void setPercorso_immagine(String percorso_immagine) {
		this.percorso_immagine = percorso_immagine;
	}
}
