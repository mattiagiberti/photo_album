import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;

public class CategoriePannello extends JPanel {

	private FramePrincipale frame;
	private Album album;

	public CategoriePannello(FramePrincipale frame, Album album) {
		this.frame = frame;
		this.album = album;
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		for(int i = 0; i < album.getSizeLista_categorie() ; i++) {
			JSeparator s = new JSeparator(JSeparator.HORIZONTAL);
			this.add(s);
			CategoriaPanel questo_panel = new CategoriaPanel(album.ritorna_cat_da_idx(i), frame, album, this, s);
			this.add(questo_panel);	
		}
		
	}

	public void aggiungiPanelCategoria(String nome) {
		JSeparator s = new JSeparator(JSeparator.HORIZONTAL);
		add(s);
		CategoriaPanel nuovo_pannello = new CategoriaPanel(album.ritorna_cat_da_nome(nome), frame, album, this, s);
		this.add(nuovo_pannello);
		revalidate();
		repaint();
	}
	
	public void rimuoviPanelCategoria(CategoriaPanel rmpannellino, JSeparator s) {
		// TODO Auto-generated method stub
		remove(rmpannellino);
		remove(s);
		revalidate();
		repaint();
		
	}
	
	
}
