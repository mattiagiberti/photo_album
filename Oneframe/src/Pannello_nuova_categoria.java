
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Pannello_nuova_categoria extends JPanel implements ActionListener{
	
	private JLabel nome_cat_lbl;
	private JTextField nome_cat_txt;
	private JLabel descrizione_cat_lbl;
	private JTextField descrizione_cat_txt;
	private JLabel password_lbl;
	private JCheckBox  pw_check;
	private JLabel aggiungi_img_default;
	private JButton sfoglia_btn;
	private JButton crea_btn;
	private Album mio_album;
	private JFrame frame;
	private FramePrincipale album_frame;
	private JPanel pannellinoCentro;
	private JPanel pannellinoSud;
	private String img_anteprima = null;
	private String password = null;
	private JLabel copertina;
	
	public Pannello_nuova_categoria(Album mio_album, JFrame frame, FramePrincipale album_frame) {
		this.frame = frame;
		this.mio_album = mio_album;
		this.album_frame = album_frame;
		
		nome_cat_lbl = new JLabel("Nome categoria: ");
		nome_cat_txt = new JTextField(25);
		descrizione_cat_lbl = new JLabel("Inserisci una descrizione: ");
		descrizione_cat_txt= new JTextField(25);
		password_lbl = new JLabel("Impostare una password? (check per confermare) ");
		pw_check = new JCheckBox();
		aggiungi_img_default = new JLabel("Aggiungi immagine di default");
		sfoglia_btn = new JButton("Sfoglia");
		crea_btn = new JButton("Crea");
		
		pannellinoSud = new JPanel();
		pannellinoCentro = new JPanel();
		
		
		this.setLayout(new BorderLayout());
		
		pannellinoCentro.add(nome_cat_lbl);
		pannellinoCentro.add(nome_cat_txt);
		pannellinoCentro.add(descrizione_cat_lbl);
		pannellinoCentro.add(descrizione_cat_txt);
		pannellinoCentro.add(password_lbl);
		pannellinoCentro.add(pw_check);
		pannellinoCentro.add(aggiungi_img_default);
		pannellinoCentro.add(sfoglia_btn);
		pannellinoSud.add(crea_btn);
		
		copertina = new JLabel();
		copertina.setSize(176, 99);
		
		this.add(pannellinoCentro, BorderLayout.CENTER);
		this.add(pannellinoSud, BorderLayout.SOUTH);
		
		sfoglia_btn.addActionListener(this);
		pw_check.addActionListener(this);
		crea_btn.addActionListener(this);
		
	}

	
	public JTextField getDescrizione_cat_txt() {
		return descrizione_cat_txt;
	}

	public void setDescrizione_cat_txt(JTextField descrizione_cat_txt) {
		this.descrizione_cat_txt = descrizione_cat_txt;
	}
	
	public void copertinaScelta(String s) {
		if(s == null) {
			return;
		}
		
		BufferedImage img = null;
		try {
		    img = ImageIO.read(new File(s));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		Image dimg = img.getScaledInstance(copertina.getWidth(), copertina.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon icona = new ImageIcon(dimg);
		
		
		copertina.setIcon(icona);
		this.pannellinoCentro.add(copertina);
		this.revalidate();
		this.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		
		if(e.getSource() == this.sfoglia_btn) {
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "png", "gif", "jpeg");
			JFileChooser scegli = new JFileChooser("C:\\Users\\");
			scegli.setFileFilter(filter);
			int i = scegli.showOpenDialog(this);    
			if(i == JFileChooser.APPROVE_OPTION)
			{
				File f = scegli.getSelectedFile();
				img_anteprima = f.getPath();
				crea_btn.setEnabled(true);
			}
			
			this.copertinaScelta(img_anteprima);
			
		}
		
		else if(e.getSource() == pw_check) {
			password = JOptionPane.showInputDialog(null, "Inserisci la password: ","Le tue foto saranno al sicuro",JOptionPane.QUESTION_MESSAGE);
			if( password == null || password.isEmpty() ) {
				JOptionPane.showMessageDialog(null, "La password � vuota", "Errore", JOptionPane.ERROR_MESSAGE);
				pw_check.setSelected(false);
			}
		}
		
		
		//bottone conferma creazione categoria
		else if(e.getSource() == this.crea_btn) {
			
			//gestione degli errori
			if(this.nome_cat_txt.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Devi dare un nome alla categoria", "Errore",JOptionPane.ERROR_MESSAGE);
			}else{
				if(mio_album.getSizeLista_categorie() != 0 && mio_album.confronta_nome_cat(this.nome_cat_txt.getText())){
					JOptionPane.showMessageDialog(null, "Questo nome � gi� stato utilizzato per un'altra categoria", "Errore", JOptionPane.ERROR_MESSAGE);
				}else {
					if(pw_check.isSelected() == false){
						//categoria senza password
						if(img_anteprima == null) {
							int yes_not_anteprima = JOptionPane.showConfirmDialog(null, "Non � stata inserita un'immagine per l'anteprima della categoria " + this.nome_cat_txt.getText() + ", continuare con una di default?", "Anteprima non selezionata", JOptionPane.YES_NO_OPTION);
							if(yes_not_anteprima == 0) {
								Categoria categoria = new Categoria(this.nome_cat_txt.getText(), this.descrizione_cat_txt.getText(), "copertina_default.jpg");
								mio_album.aggiungiCategoria(categoria);
								album_frame.aggiungiPanelCategoria(this.nome_cat_txt.getText());
								frame.dispose();
							}
						}else {
							Categoria categoria = new Categoria(this.nome_cat_txt.getText(), this.descrizione_cat_txt.getText(), img_anteprima);
							mio_album.aggiungiCategoria(categoria);
							album_frame.aggiungiPanelCategoria(this.nome_cat_txt.getText());
							frame.dispose();
						}
					}else{
						//categoria con password
						if(img_anteprima == null) {
							int yes_not_anteprima = JOptionPane.showConfirmDialog(null, "Non � stata inserita un'immagine per l'anteprima della categoria " + this.nome_cat_txt.getText() + ", continuare con una di default?", "Anteprima non selezionata", JOptionPane.YES_NO_OPTION);
							if(yes_not_anteprima == 0) {
								CategoriaProtetta categoria = new CategoriaProtetta(this.nome_cat_txt.getText(), this.descrizione_cat_txt.getText(), "lockIcon.png", password);
								mio_album.aggiungiCategoria(categoria);
								album_frame.aggiungiPanelCategoria(this.nome_cat_txt.getText());
								frame.dispose();
							}
						}else {
							CategoriaProtetta categoria = new CategoriaProtetta(this.nome_cat_txt.getText(), this.descrizione_cat_txt.getText(), img_anteprima, password);
							mio_album.aggiungiCategoria(categoria);
							album_frame.aggiungiPanelCategoria(this.nome_cat_txt.getText());
							frame.dispose();
						}
					}
				}
			}
		
		}
		
	}
}
