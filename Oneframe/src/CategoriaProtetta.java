import javax.swing.JOptionPane;

public class CategoriaProtetta extends Categoria{
	
	private String password;
	
	public CategoriaProtetta(String nome_categoria, String descrizione, String path_copertina, String password) {
		super(nome_categoria, descrizione, path_copertina);
		this.password = password;
		
	}
	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public Boolean checkPass() {
		System.out.println(password);
		
		if(password.equals(JOptionPane.showInputDialog(null, "Inserisci la password: ","Controllo password",JOptionPane.QUESTION_MESSAGE))) {
			return true;
		}else {
			return false;
		}
	}
}
