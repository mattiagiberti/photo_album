import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

public class IngrandimentoListener implements MouseListener{

	private GalleriaPannello galleriapanel;
	private String path;
	private FramePrincipale frame;
	private JPopupMenu popupmenu;
	private JMenuItem sposta;
	private JMenuItem rimuovi;
	private Categoria categoria;
	
	public IngrandimentoListener(String path, GalleriaPannello galleriapanel, FramePrincipale frame, Categoria categoria, Album album) {
		this.categoria = categoria;
		this.path = path;
		this.frame = frame;
		this.galleriapanel = galleriapanel;
		
		popupmenu = new JPopupMenu();
		rimuovi = new JMenuItem("Rimuovi immagine");
		sposta = new JMenuItem("Sposta immagine");
		
		rimuovi.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						categoria.rimuoviImmagine(path);
						galleriapanel.aggiorna();
					}
				});
		sposta.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						JComboBox<String> combo = new JComboBox<String>();
						for(int i = 0; i < album.getSizeLista_categorie(); i++) {
							if(album.ritorna_cat_da_idx(i) != categoria) {
								combo.addItem(album.ritorna_cat_da_idx(i).getNome_categoria());
							}
						}
						 
						JFrame frame = new JFrame("Scegli la categoria di destinazione");
						Pannello_sposta_cat panel = new Pannello_sposta_cat(combo, categoria, album, galleriapanel, path, frame);
						frame.add(panel);
						frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						frame.setSize(250,130);
						frame.setLocationRelativeTo(null);
						frame.setResizable(false);
						frame.setVisible(true);
						
					}
				});
		//TODO implementare sposta
		
		popupmenu.add(rimuovi);
		popupmenu.add(sposta);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		if(e.getButton() == MouseEvent.BUTTON1) {
			Ingrandimento img = new Ingrandimento(galleriapanel, path, frame);
			frame.cambiaPanello(img);
		}
		if(e.getButton() == MouseEvent.BUTTON3) {
			popupmenu.show(e.getComponent(),e.getX(),e.getY());
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}

	
