import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

public class CategoriaPanel extends JPanel implements ActionListener{

	private JLabel descrizione_label;
	private JLabel data_lbl;
	private JLabel data_creazione_label;
	private JLabel nome_categoria_label;
	private JButton rimuovi_categoria_btn;
	private JButton modifica_descrizione_btn;
	private JButton modifica_nome_btn;
	private JButton apri_btn;
	private String copertina;
	private Album album;
	private FramePrincipale frame;
	private JSeparator s;
	private Categoria categoria;
	private CategoriePannello categoriePannello;
	
	
	public CategoriaPanel(Categoria categoria, FramePrincipale frame, Album album, CategoriePannello categoriePannello, JSeparator s) {
		
		this.frame = frame;
		this.categoria = categoria;
		this.categoriePannello = categoriePannello;
		this.copertina = categoria.getCopertina();
		this.album = album;
		this.s = s;
		data_lbl = new JLabel("Data creazione:");
		descrizione_label = new JLabel(categoria.getDescrizione());
		data_creazione_label = new JLabel(categoria.getData_creazione());
		nome_categoria_label = new JLabel(categoria.getNome_categoria());
		rimuovi_categoria_btn = new JButton("Rimuovi");
		modifica_descrizione_btn = new JButton("Modifica Descrizione");
		modifica_nome_btn = new JButton("Modifica nome");
		apri_btn = new JButton("Apri");
		
		JPanel panel = new JPanel();
		
		//mostro l'immagine di anteprima
		JLabel  label_copertina = new JLabel();
		label_copertina.setSize(200,200);
		BufferedImage img = null;
		try {
		    img = ImageIO.read(new File(copertina));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		Image dimg = img.getScaledInstance(label_copertina.getWidth(), label_copertina.getHeight(), Image.SCALE_SMOOTH);
		ImageIcon icona = new ImageIcon(dimg);
		label_copertina.setIcon(icona);
		label_copertina.setLocation(20, 20);
		
		panel.add(label_copertina);
		panel.add(nome_categoria_label);
		panel.add(descrizione_label);
		panel.add(data_lbl);
		panel.add(data_creazione_label);
		panel.add(modifica_nome_btn);
		panel.add(modifica_descrizione_btn);
		panel.add(rimuovi_categoria_btn);
		panel.add(apri_btn);
		
		this.add(panel);	
		
		rimuovi_categoria_btn.addActionListener(this);
		modifica_nome_btn.addActionListener(this);
		modifica_descrizione_btn.addActionListener(this);
		apri_btn.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == rimuovi_categoria_btn) {
			if(categoria.checkPass()) {
				this.album.rimuoviCategoria(this.nome_categoria_label.getText());
				this.categoriePannello.rimuoviPanelCategoria(this, s);
			}
			
		}
		
		if(e.getSource() == this.modifica_nome_btn) {
			JFrame frame = new JFrame();
			Pannello_modifica_categoria panel_modifica = new Pannello_modifica_categoria(frame, categoria, nome_categoria_label, album, true);
			frame.add(panel_modifica);
			frame.setSize(250,130);
			frame.setLocationRelativeTo(null);
			frame.setResizable(false);
			frame.setVisible(true);
			}
		
		if(e.getSource() == this.modifica_descrizione_btn) {
			if(categoria.checkPass()) {
				JFrame frame = new JFrame();
				Pannello_modifica_categoria panel_modifica = new Pannello_modifica_categoria(frame, categoria, descrizione_label, album, false);
				frame.add(panel_modifica);
				frame.setSize(250,130);
				frame.setLocationRelativeTo(null);
				frame.setResizable(false);
				frame.setVisible(true);
			}	
		}
		
		if(e.getSource() == apri_btn) {
			if(categoria.checkPass()) {
				GalleriaPannello galleria = new GalleriaPannello(frame, categoria, categoriePannello, album);
				frame.cambiaPanello(galleria);
			}
			
		}
	}

}
