import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Pannello_modifica_categoria extends JPanel implements ActionListener{

	private JTextField txt;
	private JButton conferma;
	private JFrame frame;
	private Categoria categoria;
	private Album mio_album;
	private JLabel label_da_cambiare;
	private Boolean nomeT_descF;
	
	public Pannello_modifica_categoria(JFrame frame, Categoria categoria, JLabel label_da_cambiare, Album mio_album, boolean b) {
		this.frame = frame;
		this.categoria = categoria;
		this.mio_album = mio_album;
		this.label_da_cambiare = label_da_cambiare;
		this.nomeT_descF = b;
		
		JLabel lbl = new JLabel("Nuovo nome");
		txt = new JTextField(20);
		conferma = new JButton("Conferma");
		
		conferma.addActionListener(this);
		
		this.add(lbl);
		this.add(txt);
		this.add(conferma);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == conferma) {
			if(txt.getText() == "") {
				JOptionPane.showMessageDialog(null, "Inserire un testo", "Errore",JOptionPane.ERROR_MESSAGE);
			}else {
				if(this.nomeT_descF == true) {
					if(mio_album.confronta_nome_cat(txt.getText())) {
						JOptionPane.showMessageDialog(null, "Questo nome � gi� associato ad un'altra categoria", "Nome gi� in uso",JOptionPane.ERROR_MESSAGE);
					}else {
						categoria.cambiaNome(txt.getText());
						label_da_cambiare.setText(txt.getText()); 
						frame.dispose();
					}
				}else{
					categoria.cambiaDescrizione(txt.getText());
					label_da_cambiare.setText(txt.getText()); 
					frame.dispose();
				}
				
			}
		}
	}	
}
