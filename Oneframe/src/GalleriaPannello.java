import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GalleriaPannello extends JPanel implements ActionListener{
	
	private JButton indietro;
	private FramePrincipale frame;
	private CategoriePannello categoriePannello;
	private JPanel northpanel;
	private JPanel imglist;
	private JButton aggiungiFotobtn;
	private JButton aggiungiUrlbtn;
	private Categoria categoria;
	private Album album;
	
	public GalleriaPannello (FramePrincipale frame, Categoria categoria, CategoriePannello categoriePannello, Album album) {
		this.album = album;
		this.categoriePannello = categoriePannello;
		this.frame = frame;
		this.categoria = categoria;
		this.ridisegnaPannello();
	
	}

	public void ridisegnaPannello() {
		this.setLayout(new BorderLayout());
		
		northpanel = new JPanel();
		imglist = new JPanel();
		northpanel.setLayout(new FlowLayout());
		imglist.setLayout(new FlowLayout());
		
		indietro = new JButton("Indietro"); 
		aggiungiFotobtn = new JButton("Aggiungi Immagine");
		aggiungiUrlbtn = new JButton("Aggiungi immagine da url");
		
 		northpanel.add(indietro);
 		northpanel.add(aggiungiFotobtn);
 		northpanel.add(aggiungiUrlbtn);
 
 		String immagini[]  = categoria.listapathimmagini();
 		for(int i = 0; i < immagini.length; i++) {
 			JLabel imglabel = new JLabel();
 			imglabel.setSize(200, 150);
 			aggiungialabel(immagini[i], imglabel);
 			imglabel.addMouseListener(new IngrandimentoListener(immagini[i], this, frame, categoria, album));
 			imglist.add(imglabel);
 			 			
 		}
 		
		this.add(northpanel, BorderLayout.NORTH);
		this.add(imglist, BorderLayout.CENTER);
		
		aggiungiFotobtn.addActionListener(this);
		aggiungiUrlbtn.addActionListener(this);
		indietro.addActionListener(this);
		
	}

	public void aggiungialabel(String path, JLabel imglabel) {
		ImageIcon imgIcon = new ImageIcon();
		Boolean raised_expection = false;
		try {
			BufferedImage img = ImageIO.read(new File(path));
			Image dimg = img.getScaledInstance(imglabel.getWidth(), imglabel.getHeight(), Image.SCALE_SMOOTH);
			imgIcon = new ImageIcon(dimg);
		} catch(Exception ex) {
			raised_expection = true;
		}

		if(raised_expection) {
			try {
				URL url = new URL(path);
				Image image = ImageIO.read(url);
				image = image.getScaledInstance(imglabel.getWidth(), imglabel.getHeight(), Image.SCALE_SMOOTH);
				imgIcon = new ImageIcon(image);
			} catch(Exception ex) {
				ex.printStackTrace();
			}	
		}

		imglabel.setIcon(imgIcon);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == indietro) {
			frame.cambiaPanello(categoriePannello);
		}
		if(e.getSource()==aggiungiFotobtn) {
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "png", "gif", "jpeg");
			JFileChooser scegli = new JFileChooser("C:\\Users\\");
			scegli.setFileFilter(filter);
			int i = scegli.showOpenDialog(this);    
			if(i == JFileChooser.APPROVE_OPTION)
			{
				File f = scegli.getSelectedFile();
				this.categoria.aggiungiImg(f.getPath());
				this.aggiorna();
			}
			
		}
		if(e.getSource()==aggiungiUrlbtn) {
			String url = JOptionPane.showInputDialog(null, "Inserisci l'url: ","Aggiungi immagine",JOptionPane.QUESTION_MESSAGE);
			if(url != null && url != "") {
				this.categoria.aggiungiImg(url);
				this.aggiorna();
			}
		}
	}

	public void aggiorna() {
		this.removeAll();
		this.ridisegnaPannello();
		this.revalidate();
		this.repaint();		
	}

	
	
	
}
