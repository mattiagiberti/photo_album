import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Categoria implements Serializable {

	//private String img_anteprima;
	private ArrayList<Immagine> lista_immagini;
	private String nome_categoria;
	private String data_creazione;
	private String descrizione;
	private Immagine copertina;
	
	
	//COSTRUTTORI
	public Categoria(String nome_categoria, String descrizione, String path_copertina) {
		Date data = new Date();
		this.setData_creazione(DateFormat.getDateInstance().format(data));
		this.setNome_categoria(nome_categoria);
		this.setDescrizione(descrizione);
		copertina = new Immagine(path_copertina);
		lista_immagini = new ArrayList<Immagine>(10);
		if(path_copertina != "copertina_default.jpg" && path_copertina != "lockIcon") {
			this.aggiungiImg(path_copertina);
		}
	}
	
	/**
	 * ritorna il numero di elementi nella lista_immagini
	 * @return
	 */
	public int getSizeLista_immagini(){
		return lista_immagini.size();
	}
	
	public String getNome_categoria() {
		return nome_categoria;
	}
	
	public void setNome_categoria(String nome) {
		this.nome_categoria = nome;
	}
	
	public String getDescrizione() {
		return descrizione;
	}
	
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getData_creazione() {
		return data_creazione;
	}

	public void setData_creazione(String data) {
		this.data_creazione = data;
	}

	public String getCopertina() {
		return copertina.getPercorso_immagine();
	}

	public void cambiaNome(String nome) {
		setNome_categoria(nome);		
	}

	public void cambiaDescrizione(String text) {
		setDescrizione(text);
	}

	public void aggiungiImg(String path) {
		Immagine img = new Immagine(path);
		lista_immagini.add(img);
	}	
	
	public Boolean checkPass() {
		
		return true;
	}

	public String[] listapathimmagini() {
		String images[] = new String[lista_immagini.size()];
		for(int i = 0; i < images.length; i++) {
			images[i] = lista_immagini.get(i).getPercorso_immagine();
		}
		return images;
	}

	public void rimuoviImmagine(String path) {
		for(int i = 0; i < lista_immagini.size(); i++) {
			if(path == lista_immagini.get(i).getPercorso_immagine()) {
				lista_immagini.remove(i);
			}
		}
	}
	
	public void spostaImmagine(Categoria destinazione, Categoria sorgente, String path) {
		sorgente.rimuoviImmagine(path);
		System.out.print("da "+sorgente.nome_categoria);
		destinazione.aggiungiImg(path);
		System.out.print(" a "+destinazione.nome_categoria);
	}
}
