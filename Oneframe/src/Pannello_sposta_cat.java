import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Pannello_sposta_cat extends JPanel implements ActionListener{

	private JFrame frame;
	private JButton btn;
	private JComboBox combo;
	private Album album;
	private Categoria categoria;
	private Categoria destinazione;
	private String path;
	private GalleriaPannello galleriapanel;

	public Pannello_sposta_cat(JComboBox combo, Categoria categoria, Album album, GalleriaPannello galleriapanel, String path, JFrame frame) {
		this.categoria = categoria;
		this.galleriapanel = galleriapanel;
		this.album = album;
		this.path = path;
		this.frame = frame;
		this.combo = combo;
		btn = new JButton("conferma");
		combo.setEditable(true);
		btn.addActionListener(this);
		combo.addActionListener(this);
		
		add(combo);
		add(btn);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn) {
			String destString = (String)combo.getSelectedItem().toString();
			destinazione = album.ritorna_cat_da_nome(destString);
			if(destinazione != null) {
				categoria.spostaImmagine(destinazione, categoria, path);
				galleriapanel.aggiorna();
				frame.dispose();
			}
			
		}
	}	
}
