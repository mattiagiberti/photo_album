
public class Main {

	public static void main(String[] args) {
		
		Album mio_album= new Album();
		
		try {
			mio_album = Album.recupera();
		} catch(Exception ex) {
			ex.printStackTrace();
		}

		Runtime.getRuntime().addShutdownHook(new SaveThread(mio_album));
		
		FramePrincipale f = new FramePrincipale(mio_album);

	}

}
