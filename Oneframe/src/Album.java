import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;


public class Album implements Serializable{

	private ArrayList<Categoria> lista_categorie;
	private String nome_album;
	private static final String CONFIGFILE =".//configfile.dat";
	
	public Album() {
		lista_categorie = new ArrayList<Categoria>(10);
	}
	
	//getter & setter
	public int getSizeLista_categorie() {
		return lista_categorie.size();
	}

	public String getNome_album() {
		return nome_album;
	}

	public void setNome_album(String nome_album) {
		this.nome_album = nome_album;
	}
	
	/**
	 * Ritorna false se il nome non � ancora stato utilizzato
	 * @param nome
	 * @return
	 */
	public Boolean confronta_nome_cat(String nome) {
		if(lista_categorie.size() == 0) {
			return false;
		}
		for(Categoria c: lista_categorie) {
			if(nome.equals(c.getNome_categoria())) {
				return true;
			}
		}
		return false;
	}
	
	public Categoria ritorna_cat_da_idx(int i) {
		return lista_categorie.get(i);
	}
	
	public Categoria ritorna_cat_da_nome(String nome) {
		for(Categoria c : lista_categorie) {
			if(nome.equals(c.getNome_categoria())) {
				return c;
			}
		}
		return null;
	}

	public void aggiungiCategoria(Categoria categoria) {
		lista_categorie.add(categoria);
	}
	
	public void rimuoviCategoria(String nome) {
		Categoria categoria = ritorna_cat_da_nome(nome);
		this.lista_categorie.remove(categoria);	
	}

	public void resettaAlbum() {
		this.lista_categorie.clear();
		this.nome_album = null;
	}

	public void saveOnFile() throws IOException {
		FileOutputStream file_out = new FileOutputStream(CONFIGFILE);
		ObjectOutputStream obj_out = new ObjectOutputStream(file_out);
		obj_out.writeObject(this);
		obj_out.close();
	}
	
	@SuppressWarnings("resource")
	public static Album recupera() throws IOException, ClassNotFoundException {
		FileInputStream file_input = new FileInputStream(CONFIGFILE);
		if(file_input.available() <= 0) {
			return new Album();
		}
		ObjectInputStream oggetto_input = new ObjectInputStream(file_input);
		Album to_return = (Album)oggetto_input.readObject();
		oggetto_input.close();
		return to_return;
	}

}
