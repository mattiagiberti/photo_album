Progetto svolto per l'esame di Programmazione ad Oggetti. Traccia:

Photo Album
Progetto d’esame per l’insegnamento di Programmazione a Oggetti 2018/19
Prof. Giacomo Cabri, Nicola Capodieci
Traccia di progetto
REGOLE PER LO SVOLGIMENTO
Il progetto deve essere svolto usando il linguaggio Java e possedere i seguenti requisiti implementativi:
• Essere dotato di interfaccia grafica tramite cui interagire con il programma stesso.
• Sfruttare i meccanismi di incapsulamento, ereditarietà e polimorfismo.
• Per l’ereditarietà è possibile sfruttare classi astratte e interfacce; si considerano escluse le relazioni
di ereditarietà diretta da classi di libreria Java.
• Sfruttare le classi di sistema Java per la gestione dell'input/output.
• Utilizzare le strutture dati di libreria e i generics, motivando le scelte fatte.
• Il programma deve essere eseguibile da linea di comando.
Il software deve essere accompagnato da pagine di documentazione HTML (ivi incluse le pagine generate
tramite Javadoc o altri strumenti come Doxygen) che descrivano le scelte di progetto effettuate e la
struttura del sistema software.
Nel seguito del testo, i paragrafi evidenziati in azzurro sono facoltativi, e servono per differenziare il voto.
Lo svolgimento della parte obbligatoria contribuisce al voto per 25 punti. Il contributo delle parti facoltative
è riportato nelle rispettive descrizioni. Si noti che il punteggio massimo rimane comunque 30/30.
DESCRIZIONE DEL PROGETTO
Si vuole gestire in maniera appropriata dei file immagine presenti nel proprio hard disk. Ciò non si limita a
visualizzare la singola immagine scelta, bensì occorre organizzare le proprie fotografie/immagini in apposite
categorie di appartenenza (es: famiglia, università, amici …) e fornirne una visualizzazione accattivante.
Il presente progetto si propone di descrivere e sviluppare una applicazione Java che abbia le seguenti
funzionalità (dettagliate nei paragrafi successivi):
• Creazione di un album fotografico, diviso per categorie;
• Creazione e modifica di categorie;
• Inserimento di immagini in categorie;
• Visualizzazione di anteprime (nel senso di immagini rimpicciolite), immagini singole e slideshow.
Creazione e modifica dell’album fotografico
L’intero applicativo gestisce un solo album fotografico. Tale album fotografico è caratterizzato da
diverse categorie, un nome ed una data di creazione.
L’utente deve avere la possibilità di inserire/rimuovere/modificare categorie.
Come vedremo in seguito, l’utente può inserire immagini corrispondenti ad una categoria.Lo stato dell’album (inteso come categorie che lo compongono e immagini per singola categoria) deve
essere persistente: alla chiusura dell’applicazione, lo stato dell’album deve essere salvato su file e
ripristinato automaticamente all’avvio dell’applicazione.
Creazione e modifica di cartelle/categorie
L’utente deve poter aggiungere, togliere e rimuovere categorie dell’album fotografico.
Ciascuna categoria di foto è caratterizzata da un certo numero di foto, un nome, una data di creazione ed
una descrizione generica.
Ulteriori operazioni sulle categorie riguardano la possibilità di modificare i campi sopracitati (esclusa la data
di creazione).
Deve esserci la possibilità di proteggere l’accesso ad una categoria tramite password. Quando un utente
cerca di accedere ad una categoria protetta, una finestra di dialogo chiede all’utente una password.
L’accesso alle immagini della categoria avviene solo in caso di password correttamente inserita.
Per implementare questa caratteristica e le funzionalità collegate si suggerisce di utilizzare il
polimorfismo in Java.
Opzionalmente, si dia anche la possibilità di “unire” le foto presenti in due categorie. L’utente deve
scegliere una categoria sorgente e una di destinazione: tutte le foto della categoria sorgente verranno
spostate nella destinazione e la categoria sorgente cessa di esistere. [2 punti].
Inserimento di immagini in categorie
L’utente deve avere la possibilità di inserire immagini che afferiscono ad una categoria. Allo stesso modo le
immagini devono poter essere rimosse dalla categoria e spostate da una categoria all’altra.
Le immagini possono essere di diversi formati (png, gif, jpg, …)
L’operazione di rimozione di un’immagine da una categoria non cancella il file immagine presente sull’hard
disk.
Opzionalmente, si dia anche la possibilità di inserire immagini da URL e non solo da file su disco. [1 punto].
Visualizzazione
In una finestra iniziale, ciascuna categoria deve essere rappresentata graficamente da una immagine
rimpicciolita presa a caso dall’insieme di fotografie contenute nella categoria stessa. Si carichi una
immagine di default qualora la specifica categoria non contenga foto o sia protetta da password. Cliccando
su una categoria, si deve poter visualizzare le immagini in essa contenute.
Anche in questo caso, tutte le immagini contenute nella categoria verranno prima mostrate in formato
“anteprima” e nel caso l’utente clicchi su una di esse, comparirà l’immagine vera e propria (non
rimpicciolita).
Due pulsanti (avanti e indietro) devono permettere la navigazione all’interno della categoria.
Opzionalmente e sempre inerente alla visualizzazione, si implementi una funzionalità che permetta di
visualizzare tutte le foto a grandezza naturale contenute in una categoria tramite una slideshow: all’interno
dello stesso pannello/finestra, tutte le immagini vengono mostrate periodicamente [3 punti]